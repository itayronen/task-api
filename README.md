# task-api

## Install
You will usually install a task runner that depends on this library, like [task-run](https://gitlab.com/itayronen/task-run).  
But if you only want the api without actually running tasks:

`npm install task-api`

## Usage

```ts
import { Task, Status } from "task-api";

async function getNumberFromDatabase(key: string): Task<number> {
	// Use a task runner to create a task.
	// See: task-run - https://gitlab.com/itayronen/task-run.
}

export async function getSum(cancelToken: { onCancel: () => void }): Promise<number> {
	let sum = 0;

	let task1 = getNumberFromDatabase("key1");
	let task2 = getNumberFromDatabase("key2");

	await task1.end;
	await task2.end;

	if (task1.isSucceded) sum += await task1;
	if (task2.isSucceded) sum += await task2;

	return sum;
}
```