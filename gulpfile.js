"use strict";
let gulp = require("gulp");
let ts = require('gulp-typescript');
let del = require("del");
let merge = require("merge2");

let config = {
    tsconfig: "./src/tsconfig.json",
    tsGlob: "./src/**/*.ts",
    dest: "./lib",
};

gulp.task("build", function () {
    let tsProject = ts.createProject(config.tsconfig, { declaration: true });

    let tsResult = gulp.src(config.tsGlob)
        .pipe(tsProject());

    return merge([
        tsResult.js.pipe(gulp.dest(config.dest)),
        tsResult.dts.pipe(gulp.dest(config.dest)),
    ]);
});

gulp.task("clean", function () {
    return del([
        config.dest + '**/*',
        "./.localStorage"
    ]);
});