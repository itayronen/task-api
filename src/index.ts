export enum Status {
	Running = 1,
	Succeeded,
	Failed,
	Cancelled
}

/**
 * Represents an asynchronous operation.
 * 
 * awaiting a task, like a Promise, will resolve the result, or be rejected with the reason.
 */
export interface Task<TResult> extends Promise<TResult> {
	/**
	 * Gets the status of the task.
	 */
	readonly status: Status;

	/**
	 * Gets the error of a failed task.
	 */
	readonly error: any;

	/**
	 * Gets a promise that resolves when the task ends, regardless of it's status.
	 * 
	 * This promise will never be rejected.
	 */
	readonly end: Promise<void>;

	/**
	 * Gets whether the task has ended, regardless of it's status.
	 */
	readonly isEnded: boolean;

	/**
	 * Gets whether a task is running.
	 */
	readonly isRunning: boolean;

	/**
	 * Gets whether the task completed successfuly.
	 */
	readonly isSucceded: boolean;

	/**
	 * Gets whether the task has ended with a failure.
	 */
	readonly isFailed: boolean;

	/**
	 * Gets whether the task has ended due to a cancellation.
	 */
	readonly isCancelled: boolean;
}